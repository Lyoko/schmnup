﻿
using UnityEngine;

public abstract class BaseAvatar : MonoBehaviour
{
    [SerializeField] private float maxSpeed;
    [SerializeField] private float health;
    [SerializeField] private float maxHealth;


    public float MaxSpeed { get => maxSpeed; set => maxSpeed = value; }
    public float Health { get => health; set => health = value; }
    public float MaxHealth { get => maxHealth; set => maxHealth = value; }


    // Start is called before the first frame update
    void Start()
    {
        MaxSpeed = 10f;
    }

    // Update is called once per frame
    void Update()
    {
        
    }


  
    public void TakeDamage(float _damage)
    {
        Health -= _damage;
       
    }

    public bool Die()
    {
        return Health < 0 ? true : false; 
    }
}
