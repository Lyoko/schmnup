﻿
using UnityEngine;
using System.Xml.Serialization;
[XmlRoot("LevelDescription")]
[XmlType("LevelDescription")]
public class LevelDescription {
    //[SerializeField] public string Name;
    //[SerializeField] public EnemyDescription enemies;
    //[XmlAttribute]
    //public string name
    //{
    //    get;
    //    set;
    //}


    //[XmlArray]
    //[XmlElement("EnemyDescription", typeof(EnemyDescription))]
    //public EnemyDescription[] enemies
    //    {
    //    }
    [XmlAttribute]
    public string Name
    {
        get;
        set;
    }

    [XmlAttribute]
    public int Duration
    {
        get;
        set;
    }

    [XmlElement]
    public string Scene
    {
        get;
        set;
    }

    [XmlElement("EnemyDescription", typeof(EnemyDescription))]
    public EnemyDescription[] enemies
    {
        get;
        set;
    }

}
