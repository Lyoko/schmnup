﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class GameManager : MonoBehaviour
{

    public GameObject player;
    public GameObject enemy;
    private float currentTime;
    private float timer;
    private float cooldown = 1f;
    private Transform m_transform;
    private float min = 0.0f;
    private float max = 19.5f;
    private Vector2 screenSize;
    public static bool isBossInit;
    
    [SerializeField] private Transform boss;

    [SerializeField] private float score;
    public float Score
    {
        get => score;
        set => score = value;
    }
    //--------------------
    [SerializeField]  public TextAsset levelData;
     
   
    //-----------------------
    private static GameManager instance;
    public static GameManager Instance
    {
        get;
        private set;
       
    }

    // Start is called before the first frame update
    void Awake()
    {
        Debug.Assert(BulletFactory.Instance == null);
        GameManager.Instance = this;
        List<LevelDescription> xmlList = XmlHelpers.DeserializeDatabaseFromXML<LevelDescription>(this.levelData);
        screenSize = ScreenSize();
        m_transform = GetComponent<Transform>();
      
        Instantiate(player, new Vector3(screenSize.x * 0.1f, screenSize.y /2,-1), m_transform.rotation);
        EventManager.OnDeathEventCall += ScoreUpdate;
        //Random seed
        Random.InitState(0);
        Random.InitState((int)System.DateTime.Now.Ticks);
    }

    // Update is called once per frame
    void Update()
    {

        timer += Time.deltaTime;
        currentTime += Time.deltaTime;
        if(currentTime > cooldown)
        {
            EnemyInit();
            currentTime = 0;
        }
       
        if(timer >30 && !isBossInit)
        {
            BossInit();
            cooldown = 2f;
            isBossInit = true;
            
        }
        
       
    }
   
    public Vector2 ScreenSize()
    {

        float leftBorder;
        float rightBorder;
        float topBorder;
        float downBorder;
        //the up right corner
        Vector3 cornerPos = Camera.main.ViewportToWorldPoint(new Vector3(1f, 1f, Mathf.Abs(Camera.main.transform.position.z)));

        leftBorder = Camera.main.transform.position.x - (cornerPos.x - Camera.main.transform.position.x);
        rightBorder = cornerPos.x;
        topBorder = cornerPos.y;
        downBorder = Camera.main.transform.position.y - (cornerPos.y - Camera.main.transform.position.y);

        float width = rightBorder - leftBorder;
        float height = topBorder - downBorder;
        
        return new Vector2(width, height);
    }

    void EnemyInit()
    {
        float yPosition = Random.Range(min, max);
        Vector3 enemyPosition = new Vector3(Mathf.Floor(screenSize.x), yPosition, -1);
        //Instantiate(enemy, enemyPosition, enemy.transform.rotation);

        EnemyAvatar enemy = EnemyFactory.Instance.GetEnemy(EnemyType.EnemyA);
        enemy.gameObject.transform.position = enemyPosition;



    }
    void BossInit()
    {
        
        Instantiate(boss , new Vector3(26.94f, 10.64f, -1), m_transform.rotation);

    }
    void ScoreUpdate()
    {
        Debug.Log("score " + score);
        Score++;
    }
}
