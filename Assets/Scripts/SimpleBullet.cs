﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleBullet : Bullet
{

    private Transform m_transform;
    [SerializeField] private float playerBulletSpeed = 40f;
    [SerializeField] private float playerBulletDamage = 10f;
    [SerializeField] private float enemyBulletSpeed = 30f;
    [SerializeField] private float enemyBulletDamage = 5f;
    // Start is called before the first frame update
    void Start()
    {
        Init();
        
    }

    // Update is called once per frame
    void Update()
    {
        UpdatePosition();
    }

    public override void Init() {
        

        m_transform = GetComponent<Transform>();
        Position = m_transform.position;
        if (gameObject.tag == "PlayerBullet")
        {
            Damage = playerBulletDamage;
            //ShootModeA();

            //Debug.Log("Speed: " + Speed.x + "  " + Speed.y);
        }
        else
        {
            Damage = enemyBulletDamage;
            Speed = new Vector2(-enemyBulletSpeed, 0);
        }

    }
    public override void UpdatePosition() {
        //bulletModeC();
        m_transform.position += new Vector3(Speed.x*Time.deltaTime, Speed.y*Time.deltaTime,0);
    }

   void bulletModeC()
    {
        m_transform.position += new Vector3(Speed.x/10 * Time.deltaTime, 10 * Time.deltaTime*Mathf.Sin(Time.deltaTime), 0);
    }
}
