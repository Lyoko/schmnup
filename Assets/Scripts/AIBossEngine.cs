﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIBossEngine : MonoBehaviour
{
    public Engines engine;
    private float bigAttackTime = 5f; //TODO in future
   // private Vector2 speed = new Vector2(0, 0);
    private float timer = 0f;
    // Start is called before the first frame update
    void Start()
    {
        engine = GetComponent<Engines>();
    }

    // Update is called once per frame
    void Update()
    {
        //engine.speed = new Vector2(0, 0.2f);
        regularMovement();
    }

    void regularMovement()
    {
        timer += Time.deltaTime;
        if (timer % 10 > 5)
            engine.speed = new Vector2(0, 0.2f);
        else
            engine.speed = new Vector2(0, -0.2f);
    }
}
