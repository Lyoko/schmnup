﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIEnemyBasicEngine : MonoBehaviour
{
    private Engines m_engine;
   
    // Start is called before the first frame update
    void Start()
    {
        m_engine = GetComponent<Engines>();
    }

    // Update is called once per frame
    void Update()
    {
        m_engine.speed = new Vector2(-(float)0.2, 0);
    }

    
}
