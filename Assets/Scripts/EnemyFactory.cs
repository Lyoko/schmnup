﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyType
{
    EnemyA,
    EnemyB
}
public class EnemyFactory : MonoBehaviour
{

    private static EnemyFactory instance;
    private Transform enemyPool;
    public static EnemyFactory Instance
    {
        get;
        private set;
    }
    [SerializeField]
    private int enemyInitNumber = 10;

    private Stack<EnemyAvatar> unUsedEnemyA = new Stack<EnemyAvatar>();
    private void Awake()
    {
        Debug.Assert(EnemyFactory.Instance == null);
        EnemyFactory.Instance = this;
        enemyPool = GameObject.FindGameObjectWithTag("EnemyPool").transform;

        for (int i = 0; i < enemyInitNumber; i++)
        {
            GameObject gTmp = (GameObject)Instantiate(Resources.Load("Prefabs/poulpi"));
            EnemyAvatar enemyTmp = gTmp.GetComponent<EnemyAvatar>();
            enemyTmp.transform.parent = enemyPool;
            enemyTmp.gameObject.SetActive(false);
            unUsedEnemyA.Push(enemyTmp);
        }

    }

    public EnemyAvatar GetEnemy(EnemyType type)
    {
        EnemyAvatar enemy = null;
        switch (type)
        {
            case EnemyType.EnemyA:
                if(unUsedEnemyA.Count == 0)
                {
                    GameObject go = (GameObject)Instantiate(Resources.Load("Prefabs/poulpi"));
                    enemy = go.GetComponent<EnemyAvatar>();
                }
                else
                {
                    enemy = unUsedEnemyA.Pop();
                    enemy.gameObject.SetActive(true);
                }
                break;
            default:
                break;
        }

        return enemy;
    }

    public void ReleaseEnemy(EnemyAvatar enemy, EnemyType type)
    {
        enemy.gameObject.SetActive(false);
        switch (type)
        {
            case EnemyType.EnemyA:
                unUsedEnemyA.Push(enemy);
                break;

            default:
                break;
        }
    }
}
