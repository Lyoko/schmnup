﻿using UnityEngine.SceneManagement;
using UnityEngine;

public abstract class Bullet : MonoBehaviour
{
    [SerializeField] private float damage;
    [SerializeField] private Vector2 speed;
    [SerializeField] private Vector2 position;

    public float Damage { get => damage;  set => damage = value; }
    public Vector2 Speed { get => speed; set => speed = value; }
    public Vector2 Position { get => position; set => position = value; }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Poulpi" && gameObject.tag == "PlayerBullet")
        {

            EnemyAvatar enemyAvatar = collision.GetComponent<EnemyAvatar>();
            enemyAvatar.TakeDamage(Damage);
            if (enemyAvatar.Die())
            {
                EventManager.OnDeathEventCall();
               
                EnemyFactory.Instance.ReleaseEnemy(enemyAvatar, EnemyType.EnemyA);
            }
          
            BulletFactory.Instance.ReleaseBullet(this, BulletType.PlayerBullet);
          

        }
        else if (collision.tag == "Boss" && gameObject.tag == "PlayerBullet")
        {
            Debug.Log("Boss");
            EnemyAvatar enemyAvatar = collision.GetComponent<EnemyAvatar>();
            enemyAvatar.TakeDamage(Damage);
            if(enemyAvatar.Die())
            {

                Destroy(enemyAvatar.gameObject);
            }
            BulletFactory.Instance.ReleaseBullet(this, BulletType.PlayerBullet);
        }
        else if (collision.tag == "Player" && gameObject.tag == "EnemyBullet")
        {
            PlayerAvatar playerAvatar = collision.GetComponent<PlayerAvatar>();
            if(!playerAvatar.SuperPower)
                playerAvatar.TakeDamage(Damage);
            if (playerAvatar.Die())
            {
                Destroy(collision.gameObject);
                SceneManager.LoadScene("EndGame");
            }
            BulletFactory.Instance.ReleaseBullet(this, BulletType.EnemyBullet);
        }

    }

    public virtual void Init() {
       
    }
    public virtual void UpdatePosition() {
       
    }

    private void OnBecameInvisible()
    {
       if(gameObject.tag == "PlayerBullet")
        {
            BulletFactory.Instance.ReleaseBullet(this, BulletType.PlayerBullet);
        }
        else
        {
            BulletFactory.Instance.ReleaseBullet(this, BulletType.EnemyBullet);
        }
    }

    

}
