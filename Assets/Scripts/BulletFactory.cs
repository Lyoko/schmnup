﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum BulletType
{
    PlayerBullet,
    EnemyBullet
}

public class BulletFactory : MonoBehaviour
{
   // public GameObject go;
    private int bulletInitNumber =10;
    private Transform playerBullets;
    private Transform enemyBullets;
    private static BulletFactory instance;
    public static  BulletFactory Instance
    {
        get;
        private set;
    }

    private Stack<Bullet> unUsedPlayerBullets = new Stack<Bullet>();
    private Stack<Bullet> unUsedEnemyBullets = new Stack<Bullet>();
    private void Awake()
    {
        Debug.Assert(BulletFactory.Instance == null);
        BulletFactory.Instance = this;
        playerBullets = GameObject.FindGameObjectWithTag("PlayerBullets").GetComponent<Transform>();
        enemyBullets = GameObject.FindGameObjectWithTag("EnemyBullets").GetComponent<Transform>();


        for (int i = 0; i < bulletInitNumber;++i)
        {
            GameObject gTmp = (GameObject)Instantiate(Resources.Load("Prefabs/PlayerBullet"));
            Bullet bTmp = gTmp.GetComponent<Bullet>();
            bTmp.transform.parent = playerBullets;
            bTmp.gameObject.SetActive(false);
            unUsedPlayerBullets.Push(bTmp);
        }

        for (int i = 0; i < bulletInitNumber; ++i)
        {
            GameObject gTmp = (GameObject)Instantiate(Resources.Load("Prefabs/EnemyBullet"));
            Bullet bTmp = gTmp.GetComponent<Bullet>();
            bTmp.transform.parent = enemyBullets;
            bTmp.gameObject.SetActive(false);
            unUsedEnemyBullets.Push(bTmp);
        }


    }
  

    public Bullet GetBullet(BulletType type)
    {
        
        Bullet bullet = null;
        GameObject go = null;
        switch (type)
        {
            case BulletType.PlayerBullet:
                if (unUsedPlayerBullets.Count == 0)
                {
                   
                  go = (GameObject)Instantiate(Resources.Load("Prefabs/PlayerBullet"));
                    bullet = go.GetComponent<Bullet>();
                }
                else
                {
                    
                    bullet = unUsedPlayerBullets.Pop();
                    bullet.gameObject.SetActive(true);
                }

                break;
            case BulletType.EnemyBullet:
                if (unUsedEnemyBullets.Count == 0)
                {
                    go = (GameObject)Instantiate(Resources.Load("Prefabs/EnemyBullet"));
                    bullet = go.GetComponent<Bullet>();
                }
                else
                {
                    bullet = unUsedEnemyBullets.Pop();
                    bullet.gameObject.SetActive(true);
                }
                break;
            default:
                //throw new
                break;
        }

        return bullet;
        
    }

    public void ReleaseBullet(Bullet bullet, BulletType type)
    {
        bullet.gameObject.SetActive(false);
        //Destroy(bullet.gameObject);
        switch (type)
        {
            case BulletType.PlayerBullet:
               
                unUsedPlayerBullets.Push(bullet);
                break;
            case BulletType.EnemyBullet:
                unUsedEnemyBullets.Push(bullet);
                break;
            default:
                break;
        }
        
    }


}
