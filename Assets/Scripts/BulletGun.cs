﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletGun : MonoBehaviour
{

    [SerializeField] private float Damage;
    [SerializeField] private Vector2 Speed;
    [SerializeField] private float Cooldown;

    private float time;
    public bool outOfEnergy = false;
    private float energyChargeSpeed = 10f;
    private PlayerAvatar playerAvatar;
    private Transform bulletPosition;
    private SpriteRenderer renderer;
    private float renderTimer = 0f;
    public GameObject playerBullet;

    // Start is called before the first frame update
    void Start()
    {
        renderer = GetComponent<SpriteRenderer>();
        bulletPosition = GameObject.FindGameObjectWithTag("BulletPosition").GetComponent<Transform>();
        playerAvatar = GetComponent<PlayerAvatar>();
        Speed = new Vector2(40, 40);
        
        //playerBullet = GameObject.FindGameObjectWithTag("PlayerBullet");
    }

    // Update is called once per frame
    void Update()
    {
        if (playerAvatar.Energy <= 0)
        {
            renderTimer = 0;
            outOfEnergy = true;
        }
        if(outOfEnergy)
        {
            //if out of energy, play will flash and hcange color to red
            renderTimer += Time.deltaTime;
            renderer.color = Color.red;
            float remainder = renderTimer % 0.45f;
            renderer.enabled = remainder > 0.15f;
            EnergyCharge(energyChargeSpeed * 0.75f);
            if (playerAvatar.Energy >= 100)
            {
                renderer.color = Color.white;
                outOfEnergy = false;
            }
        }

        if (Input.GetKey(KeyCode.Space) && playerAvatar.Energy > 0 && !outOfEnergy)
            Fire();
        else if(playerAvatar.Energy < 100 && !outOfEnergy)
        {

             EnergyCharge(energyChargeSpeed);
        }

    }

    void EnergyCharge(float chargeSpeed)
    {
        
        playerAvatar.Energy += chargeSpeed * Time.deltaTime;
    }
    void Fire()
    {
        time += Time.deltaTime;
        if(time > 0.2)
        {

            playerAvatar.Energy -= 4f;
            FireModeA();
            time = 0;
        }
        
    }

    void FireModeA()
    {
        //playerBullet.GetComponent<SimpleBullet>().Speed = Speed * new Vector2(1, 0);
        Bullet bullet = BulletFactory.Instance.GetBullet(BulletType.PlayerBullet);
        
        bullet.transform.position = bulletPosition.position;
        //bullet.transform.parent = playerBullets;

        bullet.Speed = Speed * new Vector2(1, 0);
       
        //Instantiate(playerBullet, bulletPosition.position, bulletPosition.rotation, playerBullets);
    }
    //void FireModeB()
    //{
    //    playerBullet.GetComponent<SimpleBullet>().Speed = Speed;
    //    Instantiate(playerBullet, bulletPosition.position, bulletPosition.rotation, playerBullets);
    //    playerBullet.GetComponent<SimpleBullet>().Speed = Speed * new Vector2(1, -1);
    //    Instantiate(playerBullet, bulletPosition.position, bulletPosition.rotation, playerBullets);
    //    playerBullet.GetComponent<SimpleBullet>().Speed = Speed * new Vector2(1, 0);
    //    Instantiate(playerBullet, bulletPosition.position, bulletPosition.rotation, playerBullets);
    //}

    //void  FireModeC()
    //{
    //    playerBullet.GetComponent<SimpleBullet>().Speed = Speed/10 * new Vector2(1,Mathf.Cos(3*Time.deltaTime));
    //    Instantiate(playerBullet, bulletPosition.position, bulletPosition.rotation, playerBullets);
    //}

}
