﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIBasicBulletGun : MonoBehaviour
{
    private Transform m_transform;
    private Transform enemyBulletPosition;
    public GameObject enemyBullet;
    [SerializeField] private Vector2 Speed;

    private float cooldown = 1.5f;
    private float time;

    // Start is called before the first frame update
    void Start()
    {
      
        m_transform = GetComponent<Transform>();
        enemyBulletPosition = m_transform;
        Speed = new Vector2(-30, -30);
        time = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        
        if (time > cooldown)
        {
            
            if (gameObject.tag == "Boss")
                FireB();
            else
            {
                Fire();
            }
            time = 0;
        }
    }

    void FireBoss()
    {
        FireB();
    }
    //fire mode for normal enemy
    void Fire()
    {
        Bullet bullet = BulletFactory.Instance.GetBullet(BulletType.EnemyBullet);
        bullet.transform.position = enemyBulletPosition.position;
        
       // Instantiate(enemyBullet, enemyBulletPosition.position, enemyBulletPosition.rotation, enemyBullets);
    }
    //fire mode for normal boss
    void FireB()
    {
        
        Bullet bullet1 = BulletFactory.Instance.GetBullet(BulletType.EnemyBullet);
        bullet1.transform.position = enemyBulletPosition.position;
        bullet1.GetComponent<SimpleBullet>().Speed = Speed;
        Bullet bullet2 = BulletFactory.Instance.GetBullet(BulletType.EnemyBullet);
        bullet2.GetComponent<SimpleBullet>().Speed = Speed * new Vector2(1, -1);
        bullet2.transform.position = enemyBulletPosition.position;
        Bullet bullet3 = BulletFactory.Instance.GetBullet(BulletType.EnemyBullet);
        bullet3.transform.position = enemyBulletPosition.position;
        bullet3.GetComponent<SimpleBullet>().Speed = Speed * new Vector2(1, 0);
        //Instantiate(playerBullet, bulletPosition.position, bulletPosition.rotation, playerBullets);
    }
}
