﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour
{
    public delegate void OnDeathEvent();
    public static OnDeathEvent OnDeathEventCall; 
    // Start is called before the first frame update

    
    void Start()
    {
        if (OnDeathEventCall != null)
            OnDeathEventCall();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
