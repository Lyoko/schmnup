﻿
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerAvatar : BaseAvatar
{
    private float energy;
    private bool superPower  = false;
    public bool SuperPower { get => superPower; private set => superPower = value; }
    private float superPowertime = 3f;
    private float superPowerRenderTime = 0f;
    private float timer;
    private SpriteRenderer renderer;
    private InputController inputController;
	private BulletGun gun;
    public float Energy { get => energy; set => energy = value; }
    

    // Start is called before the first frame update
    void Start()
    {
        gun = GetComponent<BulletGun>();
        inputController = GetComponent<InputController>();
        renderer = GetComponent<SpriteRenderer>();
        MaxSpeed = 10f;
        Health = 100f;
        Energy = 100f;
	}

    // Update is called once per frame
    void Update()
    {

        SuperPowerCheck();

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Poulpi")
        {
            if(!SuperPower)
                TakeDamage(20);
            if (Die())
            {  
                Destroy(gameObject);
                SceneManager.LoadScene("EndGame");
            }

        }
        else if(collision.tag == "Boss")
		{
			if (!SuperPower)
				TakeDamage(40);
			if (Die())
			{
				Destroy(gameObject);
				SceneManager.LoadScene("EndGame");
			}
		}
    }

    void SuperPowerCheck()
    {
        if (inputController.IsDoubleClick && SuperPower == false && !gun.outOfEnergy)
        {
            timer = 0;
            if (Energy > 40)
            {

                Energy -= 40;

                SuperPower = true;
            }

        }
        if (SuperPower)
        {
            timer += Time.deltaTime;
            if (timer > superPowertime)
            {
                superPower = false;
                renderer.enabled = true;
                renderer.color = Color.white;
            }
            else
            {
                renderer.color = Color.yellow;
                float remainder = timer % 0.3f;
                renderer.enabled = remainder > 0.15f;
            }
           // SuperPowerRender();

        }
    }

}
