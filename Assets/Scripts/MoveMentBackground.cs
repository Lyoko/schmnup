﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveMentBackground : MonoBehaviour
{
    private Transform m_transform;
    private float speed = 0f;
  
    // Start is called before the first frame update
    void Start()
    {

        m_transform = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        UpdatePosition();
    }

    void UpdatePosition()
    {
        m_transform.position += new Vector3(-speed * Time.deltaTime,0, 0);
    }

    
}
