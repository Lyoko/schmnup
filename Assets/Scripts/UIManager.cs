﻿using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private Slider healthBar;
    private Slider energyBar;
    private Slider bossBar;
    private PlayerAvatar playerAvatar;
    private EnemyAvatar boss;
    private bool trigger;
    private Text text;

    // Start is called before the first frame update
    void Start()
    {
        text = GetComponentInChildren<Text>();
       playerAvatar = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerAvatar>();
       foreach(var t in GetComponentsInChildren<Slider>())
        {
            if (t.tag == "HealthBar")
            {
                healthBar = t;
            }
            else if (t.tag == "EnergyBar")
                energyBar = t;
            else
                bossBar = t;
        }
        bossBar.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(GameManager.isBossInit && ! trigger)
        {
            bossBar.gameObject.SetActive(true);
            boss = GameObject.FindGameObjectWithTag("Boss").GetComponent<EnemyAvatar>();
            trigger = true;
        }
        if (trigger)
            UpdateBossBar();
        UpdateHealthBar();
        UpdateEnergyBar();
        UpDateText();
        
    }
    void UpDateText()
    {
        Debug.Log("ss" + GameManager.Instance.Score.ToString());
        text.text = GameManager.Instance.Score.ToString();
    }

    void UpdateBossBar()
    {

        bossBar.value = boss.Health;
    }
    void UpdateHealthBar()
    {
        
        healthBar.value = playerAvatar.Health;
        if (healthBar.value <= 0)
            SceneManager.LoadScene("EndGame");
            
    }

    void UpdateEnergyBar()
    {
        
        energyBar.value = playerAvatar.Energy;
    }
}
