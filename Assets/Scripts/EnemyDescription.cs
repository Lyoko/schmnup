﻿

//namespace Data
//{
using UnityEngine;
using System.Xml.Serialization;
    [XmlRoot("EnemyDescription")]
    [XmlType("EnemyDescription")]
    public struct EnemyDescription 
    {
    [XmlElement]
    public int SpawnData
    {
        get;
        set;
    }
    [XmlElement]
    public Vector2 SpawnPosition
    {
        get;
        set;
    }

    [XmlElement]
    public string PrefabPath
    {
        get;
        set;
    }
        
    

    }
