﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAvatar : BaseAvatar
{

    //private bool isRendering = false;
    //private float lastTime = 0;
    //private float curtTime = 0;
    private SpriteRenderer render;
   
    private void Start()
    {
        Health = 5f;
        MaxSpeed = 5f;
        if (gameObject.tag == "Boss")
            Health = 500;
        render = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (gameObject.tag == "Poulpi" && collision.tag == "Player")
        {

            EnemyFactory.Instance.ReleaseEnemy(this, EnemyType.EnemyA);
            //Destroy(gameObject);
        }
        else if (gameObject.tag == "Boss" && collision.tag == "Player")
        {
           
            TakeDamage(40);
           
        }

   
      




    }

    public void ChangeColorWhenHurt()
    {
       
        
        render.color = Color.red;
       
    }
    private void OnBecameInvisible()
    {

        //Destroy(gameObject);
        EnemyFactory.Instance.ReleaseEnemy(this, EnemyType.EnemyA);
    }

    //private void OnWillRenderObject()
   
}
