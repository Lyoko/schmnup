﻿
using UnityEngine;

public class InputController : MonoBehaviour
{
    private Engines m_engine;
    private float lastTimeUp;
    private float lastTimeDown;
    private float lastTimeLeft;
    private float lastTimeRight;
    public bool firstClickUp;
    public bool firstClickDown;
    public bool firstClickLeft;
    public bool firstClickRight;
    private bool clickUp;
    private bool clickDown;
    private bool clickLeft;
    private bool clickRight;
    
    private bool isDoubleClick;
    public bool IsDoubleClick
    {
        get => isDoubleClick;
    }
    void Start()
    {
        m_engine = GetComponent<Engines>();
     
    }

    // Update is called once per frame
    void Update()
    {
        isDoubleClick = DoubleClick();
        //Debug.Log(isDoubleClick);
            m_engine.speed = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")); 
    }

   bool DoubleClick()
    {
        if(Input.GetAxis("Horizontal") > 0)
        {
            if((Time.time - lastTimeRight) <0.5 && clickRight)
            {
                clickRight = false;
                return true;
            }
            clickRight = false;
            firstClickRight = true;
              lastTimeRight = Time.time;
        }
        else if (Input.GetAxis("Horizontal") < 0 )
        {
            if ((Time.time - lastTimeLeft) < 0.5 && clickLeft)
            {
                clickLeft = false;
                return true;
            }
            clickLeft = false;
            firstClickLeft = true;
            lastTimeLeft = Time.time;
        }
        else if (Input.GetAxis("Vertical") < 0 )
        {
            if ((Time.time - lastTimeDown) < 0.5 && clickDown)
            {
                clickDown = false;
                return true;
            }
            clickDown = false;
            firstClickDown = true;
            lastTimeDown = Time.time;
        }
        else if (Input.GetAxis("Vertical") > 0 )
        {
            if ((Time.time - lastTimeUp) < 0.5 && clickUp)
            {
                clickUp = false;
                return true;
            }
            clickUp = false;
            firstClickUp = true;
            lastTimeUp = Time.time;
        }
        else if (Input.GetAxis("Vertical") == 0 || Input.GetAxis("Horizontal")== 0)

        {
            
            if(firstClickDown)
            {
                firstClickDown = false;
                clickDown = true;
            }
            if (firstClickUp)
            {
                firstClickUp = false;
                clickUp = true;
            }
            if (firstClickLeft)
            {
                firstClickLeft = false;
                clickLeft = true;
            }
            if (firstClickRight)
            {
                firstClickRight = false;
                clickRight = true;
            }
        }
        return false;
    }
}
