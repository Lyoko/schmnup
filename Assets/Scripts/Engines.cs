﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Engines : MonoBehaviour
{
    public Vector2 speed;
   
    public Transform  m_transform;

    public float maxSpeed;

    // Start is called before the first frame update
    void Start()
    {

        m_transform = gameObject.GetComponent<Transform>();
        maxSpeed = gameObject.GetComponent<BaseAvatar>().MaxSpeed;
        maxSpeed = 10f;
        
    }

    // Update is called once per frame
    void Update()
    {
        m_transform.position += new Vector3(speed.x*maxSpeed*Time.deltaTime,speed.y*maxSpeed*Time.deltaTime,0);
    }
}
